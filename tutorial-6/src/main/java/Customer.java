import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();

            // Determine amount for each line
            double thisAmount =  each.getThisAmount();
            totalAmount = addAmount(totalAmount, thisAmount);

            // Add frequent renter points
            frequentRenterPoints = addPointsFromRental(each, frequentRenterPoints);

            // Add bonus for a two day new release rental
            if ((each.getMovie().getPriceCode() == Movie.NEW_RELEASE) && each.getDaysRented() > 1) {
                frequentRenterPoints = addPointsFromRental(each, frequentRenterPoints);
            }

            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" + String.valueOf(thisAmount) + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints) + " frequent renter points";

        return result;
    }

    public int addPointsFromRental(Rental rental, int frequentRenterPoints) {
        return rental.addRenterPoints(frequentRenterPoints);
    }

    public double addAmount(double total, double currentAmount) {
        return total + currentAmount;
    }
}