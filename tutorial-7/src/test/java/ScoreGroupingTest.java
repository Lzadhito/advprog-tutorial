import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.junit.Test;



public class ScoreGroupingTest {

    @Test
    public void scoreGroupTest() {
        Map<Integer, List<String>> map = new HashMap<>();
        List<String> list  = new ArrayList<String>();
        list.add("Charlie");
        list.add("Foxtrot");
        map.put(11, list);
        list = new ArrayList<String>();
        list.add("Bob");
        list.add("Delta");
        list.add("Emi");
        map.put(15,list);
        list = new ArrayList<String>();
        list.add("Alice");
        map.put(12,list);

        Map<String, Integer> scores = new TreeMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        assertEquals(map, ScoreGrouping.groupByScores(scores));
    }
}