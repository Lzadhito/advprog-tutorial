package applicant;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Predicate;
import org.junit.Test;


public class ApplicantTest {

    @Test
    public void applicantTest() {
        Applicant applicant = new Applicant();
        Predicate<Applicant> creditCheck = theApplicant -> theApplicant.getCreditScore() > 600;
        Predicate<Applicant> employmentCheck = theApplicant ->
                theApplicant.getEmploymentYears() > 0;
        Predicate<Applicant> crimeCheck = theApplicant -> !theApplicant.hasCriminalRecord();


        assertEquals("Result of evaluating applicant: accepted",
                Applicant.evaluate(applicant, creditCheck));
        assertEquals("Result of evaluating applicant: accepted",
                Applicant.evaluate(applicant, creditCheck.and(employmentCheck)));
        assertEquals("Result of evaluating applicant: rejected",
                Applicant.evaluate(applicant, crimeCheck.and(employmentCheck)));
        assertEquals("Result of evaluating applicant: rejected",
                Applicant.evaluate(applicant, crimeCheck.and(creditCheck).and(employmentCheck)));
    }

}
