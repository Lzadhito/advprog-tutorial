package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;
import org.junit.Before;
import org.junit.Test;

public class PizzaStoreTest {

    private PizzaStore newYorkPizzaStore;
    private PizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testOrderPizza() {
        Pizza pizza;
        pizza = newYorkPizzaStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        pizza = newYorkPizzaStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        pizza = newYorkPizzaStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);

        pizza = depokPizzaStore.orderPizza("cheese");
        assertTrue(pizza instanceof CheesePizza);
        pizza = depokPizzaStore.orderPizza("veggie");
        assertTrue(pizza instanceof VeggiePizza);
        pizza = depokPizzaStore.orderPizza("clam");
        assertTrue(pizza instanceof ClamPizza);
    }

}
