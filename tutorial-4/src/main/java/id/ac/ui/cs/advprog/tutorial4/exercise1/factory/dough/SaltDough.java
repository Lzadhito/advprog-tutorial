package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SaltDough implements Dough {
    @Override
    public String toString() {
        return "Dough Crackers";
    }
}
