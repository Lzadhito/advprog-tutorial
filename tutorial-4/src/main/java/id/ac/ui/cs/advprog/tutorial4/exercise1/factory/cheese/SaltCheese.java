package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class SaltCheese implements Cheese {
    public String toString() {
        return "Cheese Crackers";
    }

}