package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {

    // TODO Implement me!
    // What's missing in this Singleton declaration?
    static Singleton singleObject = new Singleton();
    static String name;

    private Singleton() {
        this.name = "Single tong";
    }

    public static Singleton getInstance() {
        // TODO Implement me!
        return singleObject;
    }
}
