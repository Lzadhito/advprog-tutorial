package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    @Override
    public void performFly() {
        super.performFly();
    }

    @Override
    public void display(){}
}
