package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary){
    	this.name = name;
        if(salary < 50000) throw new IllegalArgumentException();
    	else this.salary = salary;
    	this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
