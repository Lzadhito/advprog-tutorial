package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Food {
    Food food;

    public Cucumber(Food food) {
        description = food.getDescription() + ", adding cucumber";
        this.food = food;
        //TODO Implement
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 0.40;
    }
}
