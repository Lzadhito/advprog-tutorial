package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChickenMeat extends Food {
    Food food;

    public ChickenMeat(Food food) {
        description = food.getDescription() + ", adding chicken meat";
        this.food = food;
        //TODO Implement
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 4.50;
    }
}
