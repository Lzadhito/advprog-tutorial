package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger extends Food {
    public ThinBunBurger() {
    	description = "Thin Bun Burger";
        //TODO Implement
    }

    @Override
    public double cost() {
        //TODO Implement
        return 1.50;
    }
}
