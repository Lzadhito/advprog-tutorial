package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Tomato extends Food {
    Food food;

    public Tomato(Food food) {
        description = food.getDescription() + ", adding tomato";
        this.food = food;
        //TODO Implement
    }

    @Override
    public String getDescription() {
        //TODO Implement
        return description;
    }

    @Override
    public double cost() {
        //TODO Implement
        return food.cost() + 0.50;
    }
}
