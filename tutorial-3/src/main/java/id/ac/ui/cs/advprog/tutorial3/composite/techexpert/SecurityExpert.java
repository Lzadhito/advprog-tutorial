package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    public SecurityExpert(String name, double salary){
    	this.name = name;
        if(salary < 70000) throw new IllegalArgumentException();
    	else this.salary = salary;
    	this.role = "Security Expert";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
