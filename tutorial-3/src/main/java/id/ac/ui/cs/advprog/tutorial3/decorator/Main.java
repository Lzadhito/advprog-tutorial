package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Thick Bun Burger Special cost: " + thickBunBurgerSpecialCost());
    }

    public static double thickBunBurgerSpecialCost() {
        //Thick Bun Burger with Beef Meat, Cheese, Cucumber, Lettuce, and Chili Sauce
        Food thickBunBurgerSpecial;
        thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurgerSpecial = FillingDecorator.BEEF_MEAT.addFillingToBread(thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHEESE.addFillingToBread(thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CUCUMBER.addFillingToBread(thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.LETTUCE.addFillingToBread(thickBunBurgerSpecial);
        thickBunBurgerSpecial = FillingDecorator.CHILI_SAUCE.addFillingToBread(thickBunBurgerSpecial);
        return thickBunBurgerSpecial.cost();
    }
}
