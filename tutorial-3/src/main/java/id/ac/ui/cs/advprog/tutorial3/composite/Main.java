package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.List;

public class Main {
    private static Company company;
    private static Ceo luffy;
    private static Cto zorro;
    private static BackendProgrammer franky;
    private static BackendProgrammer usopp;
    private static FrontendProgrammer nami;
    private static FrontendProgrammer robin;
    private static UiUxDesigner sanji;
    private static NetworkExpert brook;
    private static SecurityExpert chopper;

    public static void main(String args[]){
        company = new Company();
        luffy = new Ceo("Luffy", 500000.00);
        zorro = new Cto("Zorro", 320000.00);
        franky = new BackendProgrammer("Franky", 94000.00);
        usopp = new BackendProgrammer("Usopp", 200000.00);
        nami = new FrontendProgrammer("Nami",66000.00);
        robin = new FrontendProgrammer("Robin", 130000.00);
        sanji = new UiUxDesigner("sanji", 177000.00);
        brook = new NetworkExpert("Brook", 83000.00);
        chopper = new SecurityExpert("Chopper", 80000.00);
        company.addEmployee(luffy);
        company.addEmployee(zorro);
        company.addEmployee(franky);
        company.addEmployee(usopp);
        company.addEmployee(nami);
        company.addEmployee(robin);
        company.addEmployee(sanji);
        company.addEmployee(brook);
        company.addEmployee(chopper);
        System.out.println("Company total salary: " +  company.getNetSalaries());
    }

}
